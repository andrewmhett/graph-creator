//+++++++++++
//Graph Creator - A C++ implementation of a graph data structure and Djikstra's algorithm
//Andrew Hett
//6-9-2021
//------

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <cstring>
#include <limits.h>
#include "node.h"
#include "edge.h"

using namespace std;

int main(){
	map<char, Node*> nodes;
	vector<Edge*> edges;
	cout << "Graph Creator" << endl;
	cout << "COMMAND   DESCRIPTION" << endl;
	cout << "ADDV      Add a new vertex to the graph" << endl;
	cout << "ADDE      Add a new edge to the graph" << endl;
	cout << "DELV      Delete a vertex from the graph" << endl;
	cout << "DELE      Delete an edge from the graph" << endl;
	cout << "FSP       Find the shortest path between two vertices" << endl;
	cout << "PRINT     Print the adjacency table representing the graph" << endl;
	cout << "HELP      Display this message" << endl;
	cout << "QUIT      Quit the program" << endl << endl;
	while (true){
		char command_buffer[20];
		cout << "Enter a command: ";
		cin >> command_buffer;
		//shift the command to uppercase for easy case checking
		for (int i=0;i<(int)strlen(command_buffer);i++){
			command_buffer[i] = toupper(command_buffer[i]);
		}
		if (!strcmp("ADDV", command_buffer)){
			char label;
			cout << "Enter label for new vertex (single character): ";
			cin >> label;
			if (!nodes.count(toupper(label))){
				//add a new node to the map with the label as the key
				nodes[toupper(label)] = new Node();
			}else{
				cout << "A vertex with that label already exists." << endl;
			}
		}else if (!strcmp("ADDE", command_buffer)){
			char first_label;
			char second_label;
			double weight;
			cout << "Enter label of first vertex: ";
			cin >> first_label;
			cout << "Enter label of second vertex: ";
			cin >> second_label;
			first_label = toupper(first_label);
			second_label = toupper(second_label);
			bool exists = false;
			for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
				Edge* edge = *it;
				//check if the two nodes are the endpoints of the edge
				if (edge->get_first() == nodes[first_label] && edge->get_second() == nodes[second_label]){
					exists = true;
				}else if (edge->get_second() == nodes[first_label] && edge->get_first() == nodes[second_label]){
					exists = true;
				}
			}
			if (!exists){
				cout << "Enter the weight of the edge: ";
				while (!(cin >> weight)){
					cout << "Invalid input. Enter a number: ";
				}
				if (nodes.count(first_label) && nodes.count(second_label)){
					//push the new edge onto the edges vector
					edges.push_back(new Edge(nodes[first_label], nodes[second_label], weight));
				}else{
					cout << "At least one of these two vertices doesn't exist." << endl;
				}
			}else{
				cout << "An edge already exists between these vertices." << endl;
			}
		}else if (!strcmp("DELV", command_buffer)){
			char label;
			cout << "Enter label for new vertex (single character): ";
			cin >> label;
			bool all_edges_gone = false;
			while (!all_edges_gone && edges.size() > 0){
				//keep on deleting edges that connect to this node until all of them are gone
				for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
					Edge* edge = *it;
					if (edge->get_first() == nodes[toupper(label)] || edge->get_second() == nodes[toupper(label)]){
						edges.erase(it);
						delete edge;
						//break in order to avoid running past the end of the newly-sized vector
						break;
					}
					if (it == edges.end()-1){
						all_edges_gone = true;
					}
				}
			}
			Node* node = nodes[toupper(label)];
			nodes.erase(toupper(label));
			delete node;
		}else if (!strcmp("DELE", command_buffer)){
			char first_label;
			char second_label;
			cout << "Enter label of first vertex: ";
			cin >> first_label;
			cout << "Enter label of second vertex: ";
			cin >> second_label;
			first_label = toupper(first_label);
			second_label = toupper(second_label);
			for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
				Edge* edge = *it;
				//check if the two nodes are the endpoints of the edge
				if (edge->get_first() == nodes[first_label] && edge->get_second() == nodes[second_label]){
					edges.erase(it);
					delete edge;
					break;
				}else if (edge->get_second() == nodes[first_label] && edge->get_first() == nodes[second_label]){
					edges.erase(it);
					delete edge;
					break;
				}
			}
		}else if (!strcmp("FSP", command_buffer)){
			char first_label;
			char second_label;
			cout << "Enter label of first vertex: ";
			cin >> first_label;
			cout << "Enter label of second vertex: ";
			cin >> second_label;
			first_label = toupper(first_label);
			second_label = toupper(second_label);
			if (nodes.count(first_label) && nodes.count(second_label)){
				//Djikstra's algorithm
				//Reference: Wikipedia
				map<Node*, double> distances;
				map<Node*, Node*> previous;
				set<Node*> vertices;
				for (map<char, Node*>::iterator it = nodes.begin(); it != nodes.end(); ++it){
					Node* node = it->second;
					vertices.insert(node);
					distances[node] = INT_MAX;
				}
				distances[nodes[first_label]] = 0.0;
				Node* vertex = NULL;
				while (!vertices.empty()){
					double min_dist = INT_MAX;
					Node* min_node = *vertices.begin();
					for (set<Node*>::iterator it = vertices.begin(); it != vertices.end(); ++it){
						if (*it != vertex && distances[*it] < min_dist){
							min_dist = distances[*it];
							min_node = *it;
						}
					}
					vertex = min_node;
					vertices.erase(vertex);
					for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
						Edge* edge = *it;
						if (edge->get_first() == vertex){
							if (distances[edge->get_second()] > edge->get_weight()+distances[vertex]){
								distances[edge->get_second()] = edge->get_weight()+distances[vertex];
								previous[edge->get_second()] = vertex;
							}
						}else if (edge->get_second() == vertex){
							if (distances[edge->get_first()] > edge->get_weight()+distances[vertex]){
								distances[edge->get_first()] = edge->get_weight()+distances[vertex];
								previous[edge->get_first()] = vertex;
							}
						}
					}
				}
				stack<Node*> shortest_path;
				if (previous.count(nodes[second_label]) || nodes[second_label] == nodes[first_label]){
					Node* current = nodes[second_label];
					while (true){
						shortest_path.push(current);
						if (previous.count(current)){
							current = previous[current];
						}else{
							break;
						}
					}
				}
				while (!shortest_path.empty()){
					Node* current_node = shortest_path.top();
					shortest_path.pop();
					for (map<char, Node*>::iterator it = nodes.begin(); it != nodes.end(); ++it){
						if (it->second == current_node){
							cout << it->first << " ";
							break;
						}
					}
				}
				if (distances[nodes[second_label]] != INT_MAX){
					cout << endl;
					cout << "Distance: " << distances[nodes[second_label]] << endl;
				}else{
					//the distance to the destination has not been updated
					//there is no path to the destination
					cout << "No path exists between these vertices." << endl;
				}
			}else{
				cout << "At least one of these two vertices doesn't exist." << endl;
			}
		}else if (!strcmp("PRINT", command_buffer)){
			double adj_matrix[nodes.size()][nodes.size()];
			for (int i=0;i<nodes.size();i++){
				for (int o=0;o<nodes.size();o++){
					adj_matrix[i][o] = 0.0;
				}
			}
			vector<char> labels;
			int i=0;
			for (map<char, Node*>::iterator first_it = nodes.begin(); first_it != nodes.end(); ++first_it){
				Node* first_node = first_it->second;
				labels.push_back(first_it->first);
				int o=0;
				for (map<char, Node*>::iterator second_it = nodes.begin(); second_it != nodes.end(); ++second_it){
					Node* second_node = second_it->second;
					for (vector<Edge*>::iterator edge_it = edges.begin(); edge_it != edges.end(); ++edge_it){
						Edge* edge = *edge_it;
						if (edge->get_first() == first_node && edge->get_second() == second_node){
							adj_matrix[i][o] = edge->get_weight();
						}else if (edge->get_first() == second_node && edge->get_second() == first_node){
							adj_matrix[i][o] = edge->get_weight();
						}
					}
					o++;
				}
				i++;
			}
			int max_width = 4;
			for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
				char str_buf[20];
				sprintf(str_buf, "%-.2f", (*it)->get_weight());
				if (strlen(str_buf) > max_width){
					max_width = strlen(str_buf);
				}
			}
			cout << "  ";
			for (int i=0;i<(int)labels.size();i++){
				cout << labels[i];
				for (int o=0;o<max_width;o++){
					cout << " ";
				}
			}
			cout << endl;
			for (int row = 0; row<(int)nodes.size(); row++){
				cout << labels[row] << " ";
				for (int col = 0; col<(int)nodes.size(); col++){
					char str_buf[20];
					sprintf(str_buf, "%-.2f",adj_matrix[row][col]);
					cout << str_buf;
					for (int i=0;i<=max_width - strlen(str_buf);i++){
						cout << " ";
					}
				}
				cout << endl;
			}
		}else if (!strcmp("HELP", command_buffer)){
			cout << "COMMAND   DESCRIPTION" << endl;
			cout << "ADDV      Add a new vertex to the graph" << endl;
			cout << "ADDE      Add a new edge to the graph" << endl;
			cout << "DELV      Delete a vertex from the graph" << endl;
			cout << "DELE      Delete an edge from the graph" << endl;
			cout << "FSP       Find the shortest path between two vertices" << endl;
			cout << "PRINT     Print the adjacency table representing the graph" << endl;
			cout << "HELP      Display this message" << endl;
			cout << "QUIT      Quit the program" << endl;
		}else if (!strcmp("QUIT", command_buffer)){
			//free the memory allocated to the nodes and the edges
			for (map<char, Node*>::iterator it = nodes.begin(); it != nodes.end(); ++it){
				delete it->second;
			}
			for (vector<Edge*>::iterator it = edges.begin(); it != edges.end(); ++it){
				delete *it;
			}
			break;
		}else{
			cout << "Command not reconized. Use HELP for a list of commands." << endl;
		}
	}
	return 0;
}
