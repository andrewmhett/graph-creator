#ifndef EDGE_H
#define EDGE_H

#include "node.h"

class Edge{
	private:
		Node* first;
		Node* second;
		double weight;
	public:
		//getter functions
		Node* get_first();
		Node* get_second();
		double get_weight();
		//there are no setter functions
		//constructor
		Edge(Node* _first, Node* _second, double _weight);
		//there is no need for a destructor for this class
};

#endif
