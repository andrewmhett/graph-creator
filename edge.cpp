#include "edge.h"

//getter function for first node
Node* Edge::get_first(){
	return first;
}

//getter function for second node
Node* Edge::get_second(){
	return second;
}

//getter function for weight
double Edge::get_weight(){
	return weight;
}

//constructor
Edge::Edge(Node* _first, Node* _second, double _weight){
	first = _first;
	second = _second;
	weight = _weight;
}
